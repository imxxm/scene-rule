package cn.xxm.scene.dao;


import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.entity.CommonScene;
import cn.xxm.scene.service.CommonSceneService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class Part7_Gateway {

    @Autowired
    private CommonSceneDao commonSceneDao;



    @Test
    public void completeTask() {

        List<CommonScene> commonScenes = commonSceneDao.selectList(null);
        System.out.println("commonScenes = " + commonScenes);

    }

}
