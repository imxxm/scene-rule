package cn.xxm.scene.dao;


import cn.xxm.scene.entity.CommonScene;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-11-14 23:08
 **/
@SpringBootTest
public class CommonSceneDaoTest {

    @Autowired
    private CommonSceneDao commonSceneDao;

    @Test
    public void testXXX(){
        List<CommonScene> commonScenes = commonSceneDao.selectList(null);
        System.out.println("commonScenes = " + commonScenes);
    }

}
