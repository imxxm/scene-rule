package cn.xxm.scene.common.base;

public enum ExceptionCodeEum implements ResultCode{
	/**
	 * 枚举
	 */
	SYSTEM_EXCEPTION("000001","系统繁忙，请稍后再试"),
	BUISNESS_EXCEPTION("000002","业务异常"),
	USERNAME_OR_PWD_INCORRECT("200001","用户名或密码错误"),
	USER_SESSION_LOST("200002","请先登录"),
	SCENE_CODE_IS_NOT_EMPTY("200003","场景类型不能为空"),


    NEW_FILE_IS_NOT_ENPTY("200004","新配置文件不能是空文件"),
    OLD_FILE_IS_NOT_ENPTY("200005","旧配置文件不能是空文件"),
    FILE_TYPE_NOT_SUPPORT("200006","文件类型不支持"),
	;



	//操作是否成功
	boolean success;
	//操作代码
	String code;
	//提示信息
	String message;

	private ExceptionCodeEum(String code, String msg) {
		this.code = code;
		this.message = msg;
	}

	@Override
	public boolean success() {
		return success;
	}
	@Override
	public String code() {
		return code;
	}

	@Override
	public String message() {
		return message;
	}
}
