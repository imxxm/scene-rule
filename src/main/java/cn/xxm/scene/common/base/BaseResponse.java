package cn.xxm.scene.common.base;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class BaseResponse<T> implements Serializable {
	private static final long serialVersionUID = -8469420879227883186L;

	/**
	 * 返回码
	 */
	private String status = "200";
	/**
	 * 消息
	 */
	private String msg;
	/**
	 * 返回
	 */
	private T data;

	private Map meta = new HashMap();



	public BaseResponse() {

	}

	public BaseResponse(ResultCode resultCode) {
		super();
		this.status = resultCode.code();
		this.msg = resultCode.message();

		this.meta.put("status",resultCode.code());
		this.meta.put("msg",resultCode.message());
	}

	public BaseResponse( String msg, T data) {
		super();
		this.msg = msg;
		this.data = data;

		this.meta.put("status",this.status);
		this.meta.put("msg",msg);
	}

	public BaseResponse(String code, String msg, T data) {
		super();
		this.status = code;
		this.msg = msg;
		this.data = data;


		this.meta.put("status",code);
		this.meta.put("msg",msg);
	}


	public static <T> BaseResponse ok(T data) {
		BaseResponse<T> commonResponse = new BaseResponse<>();
		commonResponse.setMsg("success");
		commonResponse.setData(data);

		Map meta = new HashMap();
		meta.put("status","200");
		meta.put("msg","success");
		commonResponse.setMeta(meta);
		return commonResponse;
	}

	public static <T> BaseResponse ok() {
		BaseResponse<T> commonResponse = new BaseResponse<>();
		commonResponse.setMsg("success");


		Map meta = new HashMap();
		meta.put("status","200");
		meta.put("msg","success");
		commonResponse.setMeta(meta);
		return commonResponse;
	}

	public static <T> BaseResponse error(String msg) {
		BaseResponse<T> commonResponse = new BaseResponse<>();
		commonResponse.setMsg(msg);
		commonResponse.setStatus("9999");

		Map meta = new HashMap();
		meta.put("status","9999");
		meta.put("msg",msg);
		commonResponse.setMeta(meta);
		return commonResponse;
	}

}
