package cn.xxm.scene.dao;

import cn.xxm.scene.entity.UserToken;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface UserTokenDao extends BaseMapper<UserToken> {

    void disableClientStatus(Map inputDTO);
}
