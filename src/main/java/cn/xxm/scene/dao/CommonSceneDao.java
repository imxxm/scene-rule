package cn.xxm.scene.dao;

import cn.xxm.scene.bo.SceneBo;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.entity.CommonScene;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface CommonSceneDao extends BaseMapper<CommonScene> {

    List<SceneBo> selectSceneListByPage(@Param("page") Page page, SceneCommonInputDTO inputDTO);
}
