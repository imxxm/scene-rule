package cn.xxm.scene.dao;

import cn.xxm.scene.dto.output.MenusOutputDTO;
import cn.xxm.scene.entity.Menu;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface MenuDao extends BaseMapper<Menu> {

    List<MenusOutputDTO> selectMenuListByUserId(Long userId);
}
