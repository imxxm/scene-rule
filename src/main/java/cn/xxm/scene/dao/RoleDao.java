package cn.xxm.scene.dao;

import cn.xxm.scene.entity.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface RoleDao extends BaseMapper<Role> {

}
