package cn.xxm.scene.dao;

import cn.xxm.scene.bo.ModelExcel;
import cn.xxm.scene.bo.WhiteExcel;
import cn.xxm.scene.entity.RegistWhite;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xxm123
 * @since 2020-04-04
 */
public interface RegistWhiteDao extends BaseMapper<RegistWhite> {

    //@Select("select mobile_no mobileNo from ff_mb_regist_white where mobile_no in (#{list.mbNumber})")
    Set<String> selectExistRecordByPhone(@Param("whiteList") Set<ModelExcel> list);

    void insertBatchWhiteList(@Param("registWhiteList") Set<RegistWhite> registWhiteList);

    List<ModelExcel> selectWhiteExcel();
}
