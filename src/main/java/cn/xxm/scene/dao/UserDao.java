package cn.xxm.scene.dao;

import cn.xxm.scene.bo.UserBo;
import cn.xxm.scene.dto.input.LoginInputDTO;
import cn.xxm.scene.dto.input.RoleInputDTO;
import cn.xxm.scene.dto.input.UserInfoInputDTO;
import cn.xxm.scene.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface UserDao extends BaseMapper<User> {

    List<User> selectByConditions(LoginInputDTO inputDTO);

    List<UserBo> selectUserRoleByPage(@Param("page") Page page, UserInfoInputDTO inputDTO);

    int deleteUserAndRoleBy(String id);

    int insertUserRoleById(RoleInputDTO roleInputDTO);

    void deleteUserRoleByUserId(RoleInputDTO roleInputDTO);

}
