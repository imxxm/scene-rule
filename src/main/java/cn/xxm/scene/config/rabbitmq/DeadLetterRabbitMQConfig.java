package cn.xxm.scene.config.rabbitmq;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-08-30 14:41
 **/
@Configuration
@ConditionalOnProperty(name = {"ff.mb.rabbit.dead.letter.exchange.switch"}, havingValue = "true", matchIfMissing = false)
public class DeadLetterRabbitMQConfig {


    @Value("${ff.mb.rabbit.delay.jetco.exchange}")
    private String delayExchangeName;

    @Value("${ff.mb.rabbit.delay.routingkey}")
    private String delayRoutingkey;


    @Value("${ff.mb.rabbit.delay.jetco.queueName}")
    private String jetcoDelayQueueName;




    @Value("${ff.mb.rabbit.receive.jetco.exchange}")
    private String receiveExchangeName;


    @Value("${ff.mb.rabbit.receive.routingkey}")
    private String receiveRoutingkey;

    @Value("${ff.mb.rabbit.receive.jetco.queueName}")
    private String jetcoReceiveQueueName;




    /**
     * 功能描述:
     * @param: 死信交换机
     * @return:
     * @author:
     * @date: 2020/8/30 14:51
     */
    @Bean
    public DirectExchange userJetcoDelayExchange(){
        return new DirectExchange(delayExchangeName,true,false);
    }

    /**
     * 功能描述:
     * @param: 死信队列
     * @return:
     * @author:
     * @date: 2020/8/30 14:54
     */
    @Bean
    public Queue userJetcoDelayQueue(){
        Map<String,Object> map = new HashMap<>();
        map.put("x-dead-letter-exchange",receiveExchangeName);
        map.put("x-dead-letter-routing-key",receiveRoutingkey);
        return new Queue(jetcoDelayQueueName,true,false,false,map);
    }


    /**
     * 功能描述:
     * @param: 给死信队列绑定交换机
     * @return:
     * @author:
     * @date: 2020/8/30 14:55
     */
    @Bean
    public Binding userJetcoDelayBingding(){

        return BindingBuilder.bind(userJetcoDelayQueue()).to(userJetcoDelayExchange()).with(delayRoutingkey);
    }


    /**
     * 功能描述:
     * @param: 死信接受交换机
     * @return:
     * @author:
     * @date: 2020/8/30 14:56
     */
    @Bean
    public DirectExchange userJetcoReceiveExchange(){
        return new DirectExchange(receiveExchangeName,true,false);
    }


    /**
     * 功能描述:
     * @param: 死信接受队列
     * @return:
     * @author:
     * @date: 2020/8/30 14:59
     */
    @Bean
    public Queue userJetcoReceiveQueue(){
        return new Queue(jetcoReceiveQueueName,true,false,false);
    }



    @Bean
    public Binding userJetcoReceiveBingding(){

        return BindingBuilder.bind(userJetcoReceiveQueue()).to(userJetcoReceiveExchange()).with(receiveRoutingkey);
    }


    //@Bean
    //SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,MessageListenerAdapter listenerAdapter){
    //    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    //    container.setConnectionFactory(connectionFactory);
    //    container.setQueueNames(jetcoReceiveQueueName);
    //    container.setMessageListener(listenerAdapter);
    //
    //    // 手工确认
    //    container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
    //    return container;
    //}
    //
    //
    //@Bean
    //MessageListenerAdapter listenerAdapter(DeadLetterConsumer2 consumer){
    //    return new MessageListenerAdapter(consumer,"onMessage");
    //}


}
