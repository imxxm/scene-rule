package cn.xxm.scene.config.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class TopicMqConfig {
    private String exchangeName = "topicExchange";
    private String message = "topic.queue";

    //创建交换器
    @Bean("topicExchange")
    TopicExchange exchange() {
        return new TopicExchange(exchangeName);
    }


    //创建队列
    @Bean("topicQueue")
    public Queue queueMessage() {
        return new Queue(message);
    }


    //对列绑定并关联到ROUTINGKEY
    @Bean
    Binding bindingExchangeMessages(@Qualifier("topicQueue") Queue queueMessages, @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queueMessages).to(exchange).with("topic.#");//*表示一个词,#表示零个或多个词
    }

    @Bean
    public MessageConverter jsonMessageConverter(ObjectMapper objectMapper) {
        return new Jackson2JsonMessageConverter(objectMapper);
    }
}
