package cn.xxm.scene.config.rabbitmq;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-08-30 15:07
 **/
@Data
public class MQMessageDTO implements Serializable {
    private static final long serialVersionUID = 6045484483292388973L;


    private String reference;

    private String custNo;

    private String mobileNo ; //发送手机号

    private String email ; //

    private String event ; //

    private String noticeType ; //

    private String setSendTime ; //

    private JSONObject templateData  ; //

}
