package cn.xxm.scene.config.rabbitmq;

import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TopicSender {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send() {
        String context = "hi, i am message all";
        System.out.println("Sender : " + context);
        this.rabbitTemplate.convertAndSend("topicExchange", "topic.1", context);
    }

    public void send1() {
        JSONObject template = new JSONObject();
        template.put("Reference_no","seq1");
        template.put("customerId","9527");
        MQMessageDTO mqMessageDTO = new MQMessageDTO();

        mqMessageDTO.setReference("seq1");
        mqMessageDTO.setCustNo("9527");
        mqMessageDTO.setTemplateData(template);
        System.out.println("Sender : " + JSONObject.toJSONString(mqMessageDTO));
        this.rabbitTemplate.convertAndSend("topicExchange", "topic.message", JSONObject.toJSONString(mqMessageDTO));
    }

    public void send2() {
        String context = "hi, i am messages 2";
        System.out.println("Sender : " + context);
        this.rabbitTemplate.convertAndSend("topicExchange", "topic.messages", context);
    }
}