package cn.xxm.scene.annotation;

import cn.xxm.scene.enums.RoleEnum;

import java.lang.annotation.*;

/**
 * @author xxm
 * @version 1.0
 * @Description: 声明需要登录的注解
 * @date 2016年7月7日  上午11:34:57
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RoleAdvice {


    /**
     * 是否需要登录,默认开启
     * @return
     */
    boolean isMustLogin() default true;

    /**
     * 权限类型
     *
     * @return
     */
     RoleEnum roleType() default RoleEnum.NOTHING;
}
