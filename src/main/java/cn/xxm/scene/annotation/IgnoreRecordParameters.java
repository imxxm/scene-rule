package cn.xxm.scene.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
 
/**
 * 开关注解 - 忽略RecordParameters注解的功能
 *
 * @author xxm
 * @date 2020/02/4 13:53
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD})
public @interface IgnoreRecordParameters {
}