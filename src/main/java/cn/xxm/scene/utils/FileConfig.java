package cn.xxm.scene.utils;

import sun.awt.HKSCS;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-04-04 19:12
 **/
public class FileConfig {
    public static final List<String> UPLOAD_EXCEL_TYPES = new ArrayList<>();
    public static final List<String> PROPERTIES_TYPES = new ArrayList<>();




   static {
       UPLOAD_EXCEL_TYPES.add("csv");
       UPLOAD_EXCEL_TYPES.add("excel");
       UPLOAD_EXCEL_TYPES.add("xls");
       UPLOAD_EXCEL_TYPES.add("xlsx");

       PROPERTIES_TYPES.add("properties");
   }

}
