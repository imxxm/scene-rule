package cn.xxm.scene.utils;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-05-06 20:27
 **/
public class DiffDataUtils {

    public static String getDiffData(Object oldValue ,Object newValue){
        return "【" +oldValue +"】"+"→" +"【" +newValue +"】";
    }
}
