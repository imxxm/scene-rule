package cn.xxm.scene.controller;


import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.LoginInputDTO;
import cn.xxm.scene.dto.input.RoleInputDTO;
import cn.xxm.scene.dto.input.UserInfoInputDTO;
import cn.xxm.scene.service.UserService;
import cn.xxm.scene.validate.annotation.Validate;
import cn.xxm.scene.validate.response.FSResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@RestController
@RequestMapping("/v1")
public class UserController {


    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public FSResponseModel login(@RequestBody @Validate LoginInputDTO inputDTO){

        return userService.login(inputDTO);
    }


    @GetMapping("/users")
    public BaseResponse getUserList(UserInfoInputDTO inputDTO){
       return  userService.getUserList(inputDTO);
    }


    @PostMapping("/users")
    public BaseResponse saveUser(@RequestBody UserInfoInputDTO inputDTO){
       return  userService.saveUser(inputDTO);
    }






    @GetMapping("/users/{id}")
    public BaseResponse getUserById(@PathVariable("id") String id){
        return  userService.getUserById(id);
    }

    @PutMapping("/users/{id}")
    public BaseResponse updateById(@RequestBody UserInfoInputDTO infoInputDTO){
        return  userService.updateUserById(infoInputDTO);
    }

    @DeleteMapping("/users/{id}")
    public BaseResponse deleteById(@PathVariable("id") String id){
        return  userService.deleteUserById(id);
    }


    @PutMapping("/users/{uid}/role")
    public BaseResponse updateRoleById(@PathVariable("uid") String uid, @RequestBody RoleInputDTO roleInputDTO){
        roleInputDTO.setUid(uid);
        return  userService.updateRoleById(roleInputDTO);
    }


    @PutMapping("/users/{uid}/state/{flag}")
    public BaseResponse updateRoleById(@PathVariable("uid") String uid, @PathVariable("flag") boolean flag){
        return  userService.updateUserStatus(uid,flag);
    }


}

