package cn.xxm.scene.controller;


import cn.xxm.scene.annotation.RecordParameters;
import cn.xxm.scene.annotation.RoleAdvice;
import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@RestController
@RequestMapping("/v1")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @GetMapping("/menus")
    @RoleAdvice
    @RecordParameters
    public BaseResponse getMenus(String token){
       return  menuService.getMenus(token);
    }


}

