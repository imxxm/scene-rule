package cn.xxm.scene.controller;


import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.dto.output.DictInfoOutputDTO;
import cn.xxm.scene.entity.CommonSceneDict;
import cn.xxm.scene.service.CommonSceneDictService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@RestController
@RequestMapping("/v1/scene/dict")
public class CommonSceneDictController {

    @Autowired
    private CommonSceneDictService dictService;

    @GetMapping
    public BaseResponse getDictList(SceneCommonInputDTO inputDTO){
        return dictService.selectDictListByPage(inputDTO);
    }

    @PostMapping
    public BaseResponse saveDict(@RequestBody SceneCommonInputDTO inputDTO){
        return dictService.saveDict(inputDTO);
    }



    @GetMapping("/{id}")
    public BaseResponse getDictById(@PathVariable("id") String id){
        return  dictService.getDictById(id);
    }

    @PutMapping("/{id}")
    public BaseResponse editDictById(@PathVariable("id") String id, @RequestBody SceneCommonInputDTO inputDTO){
        inputDTO.setId(id);
        return  dictService.editDictById(inputDTO);
    }


    @PutMapping("/{did}/state/{flag}")
    public BaseResponse editDictById(@PathVariable("did") String did, @PathVariable("flag") boolean flag){
        return  dictService.updateDictStatus(did,flag);
    }


    @DeleteMapping("/{id}")
    public BaseResponse deleteById(@PathVariable("id") String id){
        return  dictService.deleteDictById(id);
    }

}

