package cn.xxm.scene.controller;


import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.entity.Role;
import cn.xxm.scene.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@RestController
@RequestMapping("/v1/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping
    private BaseResponse getRoleList(){
        List<Role> roles = roleService.selectList(null);
        return BaseResponse.ok(roles);
    }

}

