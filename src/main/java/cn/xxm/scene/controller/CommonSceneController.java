package cn.xxm.scene.controller;


import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.service.CommonSceneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@RestController
@RequestMapping("/v1/scene")
@Api(tags = "通用场景服务")
public class CommonSceneController {


    @Autowired
    private CommonSceneService sceneService;

    @GetMapping
    @ApiOperation(value = "获取场景List列表")
    public BaseResponse getSceneList(SceneCommonInputDTO inputDTO){
        return sceneService.selectSceneListByPage(inputDTO);
    }

    @PostMapping
    public BaseResponse saveScene(@RequestBody SceneCommonInputDTO inputDTO){
        return sceneService.saveScene(inputDTO);
    }



    @GetMapping("/{id}")
    public BaseResponse getSceneById(@PathVariable("id") String id){
        return  sceneService.getSceneById(id);
    }

    @PutMapping("/{id}")
    public BaseResponse editSceneById(@PathVariable("id") String id, @RequestBody SceneCommonInputDTO inputDTO){
        inputDTO.setId(id);
        return  sceneService.editSceneById(inputDTO);
    }


    @PutMapping("/{did}/{field}/{flag}")
    public BaseResponse editSceneById(@PathVariable("did") String did,@PathVariable("field") String field, @PathVariable("flag") boolean flag){
        return  sceneService.updateSceneStatus(did,field,flag);
    }


    @DeleteMapping("/{id}")
    public BaseResponse deleteById(@PathVariable("id") String id){
        return  sceneService.deleteSceneById(id);
    }


    @PutMapping("/{id}/allocateRules")
    public BaseResponse allocateRules(@PathVariable("id") String id,@RequestBody SceneCommonInputDTO inputDTO){
        inputDTO.setId(id);
        return  sceneService.allocateRules(inputDTO);
    }
}

