package cn.xxm.scene.controller;

import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.config.rabbitmq.DeadLetterRabbitMQProducer;
import cn.xxm.scene.config.rabbitmq.MQMessageDTO;
import cn.xxm.scene.config.rabbitmq.TopicSender;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-08-30 15:16
 **/

@RestController
@RequestMapping("/rabbit")
public class TestRabbitMQController {



    @Autowired
    private DeadLetterRabbitMQProducer producer;

    @GetMapping("/test/{custNo}")
    public BaseResponse test(@PathVariable("custNo") String custNo){
        for (int i = 0; i <10000 ; i++) {
            JSONObject template = new JSONObject();
            template.put("Reference_no","seq1");
            template.put("customerId",custNo);
            MQMessageDTO mqMessageDTO = new MQMessageDTO();

            mqMessageDTO.setReference("seq1");
            mqMessageDTO.setCustNo(custNo);
            mqMessageDTO.setTemplateData(template);
            producer.sendMsg(mqMessageDTO);
        }


        return  BaseResponse.ok();
    }

    @Autowired
    private TopicSender topicSender;

    @GetMapping("/topic/{type}")
    public BaseResponse testType(@PathVariable("type") String type){
        topicSender.send();
        return  BaseResponse.ok();
    }

}
