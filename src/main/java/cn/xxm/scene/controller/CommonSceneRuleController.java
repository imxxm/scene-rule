package cn.xxm.scene.controller;


import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.service.CommonSceneCheckRuleService;
import cn.xxm.scene.service.CommonSceneDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@RestController
@RequestMapping("/v1/scene/rule")
public class CommonSceneRuleController {
    @Autowired
    private CommonSceneCheckRuleService ruleService;

    @GetMapping
    public BaseResponse getRuleList(SceneCommonInputDTO inputDTO){
        return ruleService.selectRuleListByPage(inputDTO);
    }

    @PostMapping
    public BaseResponse saveRule(@RequestBody SceneCommonInputDTO inputDTO){
        return ruleService.saveRule(inputDTO);
    }



    @GetMapping("/{id}")
    public BaseResponse getRuleById(@PathVariable("id") String id){
        return  ruleService.getRuleById(id);
    }

    @PutMapping("/{id}")
    public BaseResponse editRuleById(@PathVariable("id") String id, @RequestBody SceneCommonInputDTO inputDTO){
        inputDTO.setId(id);
        return  ruleService.editRuleById(inputDTO);
    }


    @PutMapping("/{did}/state/{flag}")
    public BaseResponse editRuleById(@PathVariable("did") String did, @PathVariable("flag") boolean flag){
        return  ruleService.updateRuleStatus(did,flag);
    }


    @DeleteMapping("/{id}")
    public BaseResponse deleteById(@PathVariable("id") String id){
        return  ruleService.deleteRuleById(id);
    }






}

