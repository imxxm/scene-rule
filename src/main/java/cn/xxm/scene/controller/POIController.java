//package cn.xxm.scene.controller;
//
//import cn.afterturn.easypoi.excel.ExcelImportUtil;
//import cn.afterturn.easypoi.excel.entity.ImportParams;
//import cn.xxm.scene.bo.WhiteExcel;
//import cn.xxm.scene.constant.Constants;
//import cn.xxm.scene.entity.RegistWhite;
//import cn.xxm.scene.service.RegistWhiteService;
//import cn.xxm.scene.service.UserService;
//import cn.xxm.scene.utils.ExcleUtils;
//import cn.xxm.scene.utils.FileConfig;
//import com.baomidou.mybatisplus.toolkit.IdWorker;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.collections.CollectionUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.multipart.MultipartFile;
//import sun.rmi.runtime.Log;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletResponse;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
////@Controller
//@Slf4j
//public class POIController {
//
//    @Autowired
//    private RegistWhiteService registWhiteService;
//
//    /**
//     * 首页
//     *
//     * @return
//     */
//    @GetMapping(value = {"/index", "/"})
//    public String index() {
//        return "index";
//    }
//
//
//    /**
//     * excle导入
//     *
//     * @param file
//     * @param
//     * @return
//     */
//    @PostMapping("/upload")
//    @ResponseBody
//    public String upload(@RequestParam("fileName") MultipartFile file) {
//        long startTime = System.currentTimeMillis();
//        if (file.isEmpty()) {
//            return "空文件";
//        }
//        //获取文件名
//        String fileName = file.getOriginalFilename();
//        //获取文件后缀
//        String type = fileName.indexOf(".") != -1 ? fileName.split("[.]")[1] : null;
//        //文件格式判断
//        if (!FileConfig.UPLOAD_EXCEL_TYPES.contains(type.toLowerCase())) {
//            return "文件类型不对";
//        }
//        //
//        ImportParams params = new ImportParams();
//        //表格标题所占据的行数,默认0，代表没有标题
//        params.setTitleRows(1);
//        //表头所占据的行数行数,默认1，代表标题占据一行
//        params.setHeadRows(1);
//        List<WhiteExcel> list;
//        try {
//            // 1. 解析excel的数据
//            list = ExcelImportUtil.importExcel(file.getInputStream(), WhiteExcel.class, params);
//            long step1 = System.currentTimeMillis();
//            log.info(">>>step1 cost 【{}ms】", (step1 - startTime));
//            insertBatchWhiteList(list);
//            long step2 = System.currentTimeMillis();
//            log.info(">>>step2 cost 【{}ms】", (step2 - startTime));
//            return "success";
//        } catch (Exception e) {
//            e.printStackTrace();
//            return e.getMessage();
//        }
//    }
//
//    private void insertBatchWhiteList(List<WhiteExcel> whiteExcelList) {
//        // 2. 导入数据库
//        // 2.1 查询有多少mobileNo重复的
//        List<String> existMobileList = registWhiteService.selectExistRecordByPhone(whiteExcelList);
//        log.info(">>>exist mobile phone : {}", existMobileList);
//        List<RegistWhite> registWhiteList = new ArrayList<>();
//        whiteExcelList.forEach(item -> {
//            if (!existMobileList.contains(item.getMbNumber())) {
//                RegistWhite registWhite = new RegistWhite();
//                registWhite.setId(IdWorker.getId());
//                registWhite.setMobileNo(item.getMbNumber());
//                registWhite.setIsSeed(Constants.WHITE_REGISTER_SEED_USER);
//                registWhite.setIsLeaf(Constants.WHITE_REGISTER_LEAF);
//                registWhite.setCreateDate(new Date());
//                registWhite.setUpdateDate(new Date());
//                registWhite.setStatus("1");
//                registWhite.setEmail(item.getEmail());
//                registWhiteList.add(registWhite);
//            }
//        });
//        log.info(">>>有效记录条数:{}",registWhiteList.size());
//        if(CollectionUtils.isNotEmpty(registWhiteList)){
//            registWhiteService.insertBatchWhiteList(registWhiteList);
//        }
//    }
//
//
//    /**
//     * excle下载
//     *
//     * @param title
//     * @param response
//     */
//    @GetMapping("/download")
//    @ResponseBody
//    public void downLoadExcel(String title, HttpServletResponse response) {
//        String sheetName = "白名单";
//        long startTime = System.currentTimeMillis();
//        //List<WhiteExcel> whiteExcelList = getMockWhiteExcel();
//        List<WhiteExcel> whiteExcelList = getRegisterWhiteExcel();
//        long step1 = System.currentTimeMillis();
//        log.info(">>>step1 cost 【{}ms】", (step1 - startTime));
//        ExcleUtils.downloadExcel(whiteExcelList, title, sheetName, WhiteExcel.class, title + ".csv", response);
//        long step2 = System.currentTimeMillis();
//        log.info(">>>step2 cost 【{}ms】", (step2 - startTime));
//    }
//
//
//    /**
//     * excle下载
//     *
//     * @param title
//     * @param response
//     */
//    @GetMapping("/downloadTemplate")
//    @ResponseBody
//    public void downloadTemplate(String title, HttpServletResponse response) {
//        String sheetName = "白名单";
//        long startTime = System.currentTimeMillis();
//        //List<WhiteExcel> whiteExcelList = getMockWhiteExcel();
//        List<WhiteExcel> whiteExcelList = new ArrayList<>();
//        long step1 = System.currentTimeMillis();
//        log.info(">>>step1 cost 【{}ms】", (step1 - startTime));
//        ExcleUtils.downloadExcel(whiteExcelList, title, sheetName, WhiteExcel.class, title + ".csv", response);
//        long step2 = System.currentTimeMillis();
//        log.info(">>>step2 cost 【{}ms】", (step2 - startTime));
//    }
//
//    private List<WhiteExcel> getRegisterWhiteExcel() {
//        List<WhiteExcel> whiteExcelList = registWhiteService.selectWhiteExcel();
//        System.out.println("whiteExcelList.size() = " + (null == whiteExcelList ? 0 : whiteExcelList.size()));
//        return whiteExcelList;
//    }
//
//    private List<WhiteExcel> getMockWhiteExcel() {
//        List<WhiteExcel> whiteExcelList = new ArrayList<>();
//        for (int i = 1; i <= 50000; i++) {
//            WhiteExcel whiteExcel = new WhiteExcel();
//            whiteExcel.setAppVersion(i);
//            whiteExcel.setMbNumber("00852" + i);
//            whiteExcel.setEmail("00852" + i + "@qq.com");
//            whiteExcelList.add(whiteExcel);
//        }
//        return whiteExcelList;
//    }
//
//
//}