package cn.xxm.scene;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableSwagger2Doc
@MapperScan("cn.xxm.scene.dao")
public class SceneRuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SceneRuleApplication.class, args);
    }

}
