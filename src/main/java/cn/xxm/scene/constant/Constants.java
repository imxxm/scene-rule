package cn.xxm.scene.constant;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-04-04 21:17
 **/
public class Constants {

    public static final String WHITE_REGISTER_SEED_USER = "1";
    public static final String WHITE_REGISTER_LEAF = "0";
}
