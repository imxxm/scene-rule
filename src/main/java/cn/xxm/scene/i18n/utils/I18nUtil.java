//package cn.xxm.scene.i18n.utils;
//
//import cn.xxm.scene.utils.SpringContextUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.MessageSource;
//import org.springframework.context.i18n.LocaleContextHolder;
//
///**
// * @program: scene-rule
// * @link: 55864455@qq.com
// * @author: Mr.Xxm
// * @create: 2020-11-23 16:35
// **/
//@Slf4j
//public class I18nUtil {
//
//    private static MessageSource messageSource;
//
//    static {
//        try {
//            messageSource = (MessageSource) SpringContextUtil.getBean("xxmMessageSource");
//        }catch (Exception e){
//            log.info(">>>init xxmMessageSource fail",e);
//        }
//    }
//
//    /**
//     * 功能描述:通过国际化配置文件的key值获取value
//     * @param:
//     * @return:
//     * @author:
//     * @date: 2020/11/23 16:56
//     */
//    public static String getI18nValue(String i18nKey){
//        try {
//            return messageSource == null? null : messageSource.getMessage(i18nKey,null, LocaleContextHolder.getLocale());
//        } catch (Exception e) {
//            log.warn(">>>获取{}对应的国际化消息异常-> {} ",i18nKey,e.getMessage());
//            return null;
//        }
//    }
//
//
//    public static String paramGetI18nValue(String i18nKey,String ... params){
//          return messageSource == null? null : messageSource.getMessage(i18nKey,params, LocaleContextHolder.getLocale());
//    }
//}
