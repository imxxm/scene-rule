package cn.xxm.scene.exception;


import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.common.base.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {



    //@Autowired
    //private KafkaSender<JSONObject> kafkaSender;
    //
    //@ExceptionHandler(RuntimeException.class)
    //public JSONObject exceptionHandler(Exception e) {
    //    log.info("###全局捕获异常###,error:{}", e);
    //
    //    // 1.封装异常日志信息
    //    JSONObject errorJson = new JSONObject();
    //    JSONObject logJson = new JSONObject();
    //    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
    //    logJson.put("request_time", df.format(new Date()));
    //    logJson.put("error_info", e);
    //    errorJson.put("error", logJson);
    //    kafkaSender.send(errorJson);
    //    // 2. 返回错误信息
    //    JSONObject result = new JSONObject();
    //    result.put("code", 500);
    //    result.put("msg", "系统错误");
    //
    //    return result;
    //}

	@ExceptionHandler(value = CommonException.class)
	public Object handleCommonException(CommonException e){
		ResultCode resultCode = e.getResultCode();
		String str = "\n\tresultCode.getMessage() → " + resultCode.message()+"\n";
		log.error(" GlobalExceptionHandler#handleCommonException has been invoke ↓ {}",str,e);
		String code = resultCode.code();
		String message = resultCode.message();

		return new BaseResponse<>(code,message,null);
	}

}