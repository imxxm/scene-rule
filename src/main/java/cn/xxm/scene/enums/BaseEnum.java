package cn.xxm.scene.enums;

/**
 * @author xxm
 * @create 2020-01-04 14:39
 */
public interface BaseEnum {
    String code = null;
    String name = null;

    String getCode();

    String getName();

}