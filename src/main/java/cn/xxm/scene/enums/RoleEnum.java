package cn.xxm.scene.enums;

import cn.xxm.scene.utils.EnumUtil;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum RoleEnum implements BaseEnum{
    /**
     */
    NOTHING("000","nothing","need to do nothing");


    private String roleCode;
    private String roleName;
    private String desc;

    RoleEnum() {
    }
    RoleEnum(String roleCode, String roleName, String desc) {
        this.roleCode = roleCode;
        this.roleName = roleName;
        this.desc = desc;
    }


    @Override
    public String getCode() {
        return this.roleCode;
    }

    @Override
    public String getName() {
        return this.roleName;
    }




    public static void main(String[] args) {
        RoleEnum byCode = EnumUtil.getByCode("000", RoleEnum.class);
        System.out.println("byCode = " + byCode);
    }
}