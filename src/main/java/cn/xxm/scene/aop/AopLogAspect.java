//package cn.xxm.scene.aop;
//
//import cn.xxm.scene.elk.KafkaSender;
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Pointcut;
//import org.aspectj.lang.reflect.MethodSignature;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.HttpServletRequest;
//import java.lang.reflect.Method;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * aop切面
// */
////@Aspect
////@Component
//public class AopLogAspect {
//	@Autowired
//	private KafkaSender<JSONObject> kafkaSender;
//
//	@Autowired
//    private RabbitTemplate rabbitTemplate;
//
//    /** 参数名发现器 */
//    private static final LocalVariableTableParameterNameDiscoverer PARAMETER_NAME_DISCOVER = new LocalVariableTableParameterNameDiscoverer();
//    /** 无返回值 */
//    private static final String VOID_STRING = "void";
//
//
//	// 申明一个切点 里面是 execution表达式
//	@Pointcut("execution(* cn.xxm.*.service.impl.*.*(..))")
//	private void serviceAspect() {
//	}
//
//	// 请求method前打印内容
//	@Around(value = "serviceAspect()")
//	public Object methodBefore(ProceedingJoinPoint joinPoint) throws Throwable {
//		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
//				.getRequestAttributes();
//		HttpServletRequest request = requestAttributes.getRequest();
//
//        JSONObject jsonObject = new JSONObject();
//        JSONObject requestJsonObject = new JSONObject();
//        JSONObject responseJsonObject = new JSONObject();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
//        // 获取目标Class
//        Object targetObj = joinPoint.getTarget();
//        Class<?> targetClazz = targetObj.getClass();
//        // 获取目标method
//        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
//        Method targetMethod = methodSignature.getMethod();
//        boolean shouldRecordInputParams = true;
//        if (shouldRecordInputParams) {
//            /// log.info("【the way in】 Class#Method → {}#{}", targetClazz.getName(), targetMethod.getName());
//            Object[] parameterValues = joinPoint.getArgs();
//            if (parameterValues != null && parameterValues.length > 0) {
//                String[] parameterNames = PARAMETER_NAME_DISCOVER.getParameterNames(targetMethod);
//                if (parameterNames == null) {
//                    throw new RuntimeException("parameterNames must not be null!");
//                }
//                StringBuilder sb = new StringBuilder(8);
//                sb.append("url").append(" => ").append(request.getRequestURL()).append("  ");
//                int iterationTimes = parameterValues.length;
//                for (int i = 0; i < iterationTimes; i++) {
//                    sb.append("\t").append(parameterNames[i]).append(" => ").append(jsonPretty(parameterValues[i]));
//                    if (i < iterationTimes - 1) {
//                        sb.append("\n");
//                    }
//                }
//                requestJsonObject.put("request_args", sb.toString());
//                requestJsonObject.put("request_time", df.format(new Date()));
//                jsonObject.put("request", requestJsonObject +" ");
//            }
//        }
//
//
//
//
//        Object proceed = joinPoint.proceed();
//
//        boolean shouldRecordOutputParams = true;
//        if (shouldRecordOutputParams) {
//            Class<?> returnClass = targetMethod.getReturnType();
//
//            /// log.info("【the way out】 ReturnType → {}", targetMethod.getReturnType());
//            if (VOID_STRING.equals(returnClass.getName())) {
//                return proceed;
//            }
//            responseJsonObject.put("response_content",jsonPretty(proceed));
//            responseJsonObject.put("response_time", df.format(new Date()));
//            jsonObject.put("response", responseJsonObject);
//        }
//
//
//
//
//        kafkaSender.send(jsonObject);
//        String msg = JSON.toJSONString(jsonObject);
//        rabbitTemplate.convertAndSend("ex_logstash","logstash",msg);
//        return proceed;
//	}
//
//
//
//    /**
//     * json格式化输出
//     *
//     * @param obj
//     *         需要格式化的对象
//     * @return json字符串
//     * @date 2019/12/5 11:03
//     */
//    String jsonPretty(Object obj) {
//        return JSON.toJSONString(obj);
//    }
//
//}
