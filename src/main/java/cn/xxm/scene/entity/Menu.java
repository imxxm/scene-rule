package cn.xxm.scene.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@TableName("ff_mb_menu")
@Data
@ToString
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 菜单名称
     */
    @TableField("menu_name")
    private String menuName;
    /**
     * 菜单描述
     */
    @TableField("menu_desc")
    private String menuDesc;
    /**
     * 菜单深度
     */
    @TableField("menu_deep")
    private Integer menuDeep;
    /**
     * 是否叶子菜单
     */
    @TableField("is_leaf")
    private Integer isLeaf;
    /**
     * 父级菜单,一级菜单的父级菜单为0
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 菜单跟路由
     */
    private Long path;


}
