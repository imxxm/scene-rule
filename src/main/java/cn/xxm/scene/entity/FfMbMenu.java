package cn.xxm.scene.entity;


import java.io.Serializable;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.ToString;

/**
 * @Author xxm 
**/
@TableName(value = "ff_mb_menu")
@Data
@ToString
public class FfMbMenu implements Serializable {

  private static final long serialVersionUID = 1L;

  /*** null */
  private Long id;

  /*** 菜单名称 */
  private String menuName;

  /*** 菜单描述 */
  private String menuDesc;

  /*** 菜单深度 */
  private Long menuDeep;

  /*** 是否叶子菜单 */
  private Long isLeaf;

  /*** 父级菜单,一级菜单的父级菜单为0 */
  private Long parentId;

  /*** 后台访问url */
  private String path;















}
