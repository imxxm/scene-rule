package cn.xxm.scene.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@TableName("ff_mb_common_scene_dict")
@Data
@ToString
public class CommonSceneDict implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 场景code
     */
    @TableField("scene_code")
    private String sceneCode;
    /**
     * 场景名称
     */
    @TableField("scene_name")
    private String sceneName;

    /**
     * 状态:0-不可用;1:可用
     */
    //@JsonProperty("state")
    @JSONField(name="state")
    private String status;

    @TableField("create_date")
    private Date createDate;

    @TableField("update_date")
    private Date updateDate;

}
