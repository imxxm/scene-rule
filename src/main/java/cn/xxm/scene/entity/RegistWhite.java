package cn.xxm.scene.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Xxm123
 * @since 2020-04-04
 */
@TableName("ff_mb_regist_white")
@Data
@ToString
public class RegistWhite implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @TableField("mobile_no")
    private String mobileNo;
    /**
     * 是否种子用户:0:否;1:是
     */
    @TableField("is_seed")
    private String isSeed;
    /**
     * 被推荐人;种子用户没有被推荐人
     */
    @TableField("parent_id")
    private Long parentId;
    /**
     * 是否叶子结点:0-否;1-是
     */
    @TableField("is_leaf")
    private String isLeaf;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("update_date")
    private Date updateDate;
    /**
     * 是否有效:0-无效;1有效  默认有效
     */
    private String status;
    /**
     * 邮件
     */
    private String email;
    /**
     * 创建人
     */
    @TableField("created_by")
    private String createdBy;
    /**
     * 修改人
     */
    @TableField("updated_by")
    private String updatedBy;



}
