package cn.xxm.scene.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@TableName("ff_mb_role")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 角色code
     */
    @TableField("role_code")
    private String roleCode;
    @TableField("role_name")
    private String roleName;
    /**
     * 角色描述
     */
    @TableField("role_desc")
    private String roleDesc;
    /**
     * 状态
     */
    private String status;
    @TableField("update_date")
    private Date updateDate;
    @TableField("create_date")
    private Date createDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "Role{" +
        ", id=" + id +
        ", roleCode=" + roleCode +
        ", roleName=" + roleName +
        ", roleDesc=" + roleDesc +
        ", status=" + status +
        ", updateDate=" + updateDate +
        ", createDate=" + createDate +
        "}";
    }
}
