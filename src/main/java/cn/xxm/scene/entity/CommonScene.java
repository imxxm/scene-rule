package cn.xxm.scene.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@TableName("ff_mb_common_scene")
@Data
@ToString
public class CommonScene implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 场景标识
     */
    @TableField("scene_code")
    private String sceneCode;
    /**
     * 场景名称
     */
    @TableField("scene_name")
    private String sceneName;
    /**
     * 场景描述
     */
    @TableField("scene_desc")
    private String sceneDesc;
    /**
     * 是否信任设备
     */
    @TableField("is_trust_device")
    private Integer isTrustDevice;
    /**
     * 登录类型:0001-账密;0002-手势;0004-指纹&人脸
     */
    @TableField("login_type")
    private String loginType;
    /**
     * 金额下限
     */
    @TableField("amount_lower_limmit")
    private BigDecimal amountLowerLimmit;
    /**
     * 金额上限
     */
    @TableField("amount_upper_limmit")
    private BigDecimal amountUpperLimmit;
    /**
     * request_uri 校验
     */
    @TableField("invoke_request_uri")
    private String invokeRequestUri;
    /**
     * 用户标识:0-个人版;1-企业版
     */
    @TableField("user_flag")
    private String userFlag;
    /**
     * 用户类型:0-低门槛;2-高门槛
     */
    @TableField("user_type")
    private String userType;
    /**
     * 规则
     */
    @TableField("rule_code_list")
    private String ruleCodeList;

    /**
     * 状态:0-无效;1-有效
     * 默认1
     */
    private String state;



}
