package cn.xxm.scene.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@TableName("ff_mb_common_scene_check_rule")
@Data
@ToString
public class CommonSceneCheckRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * rule标识
     */
    @TableField("rule_code")
    private String ruleCode;
    /**
     * 检查要素如:SMS_OTP;LIVENESS
     */
    @TableField("check_rule_key")
    private String checkRuleKey;
    /**
     * 校验值(value)
     */
    @TableField("check_rule_value")
    private String checkRuleValue;
    /**
     * 校验描述
     */
    @TableField("check_rule_desc")
    private String checkRuleDesc;
    /**
     * 备注
     */
    private String remark;
    /**
     * 目标处理类 serviceId
     */
    @TableField("target_handler")
    private String targetHandler;

    private boolean state;
}
