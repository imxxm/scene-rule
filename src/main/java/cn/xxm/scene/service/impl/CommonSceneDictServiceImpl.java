package cn.xxm.scene.service.impl;

import cn.xxm.scene.bo.DictBo;
import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.dto.output.DictInfoOutputDTO;
import cn.xxm.scene.entity.CommonSceneDict;
import cn.xxm.scene.dao.CommonSceneDictDao;
import cn.xxm.scene.entity.User;
import cn.xxm.scene.service.CommonSceneDictService;
import cn.xxm.scene.utils.XxmBeanUtils;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@Service
public class CommonSceneDictServiceImpl extends ServiceImpl<CommonSceneDictDao, CommonSceneDict> implements CommonSceneDictService {


    @Autowired
    private CommonSceneDictDao dictDao;

    @Override
    public BaseResponse selectDictListByPage(SceneCommonInputDTO inputDTO) {
        Page page = new Page<>(inputDTO.getPagenum(), inputDTO.getPagesize());

        List<DictBo> sceneDicts = dictDao.selectDictListByPage(page, inputDTO);

        DictInfoOutputDTO outputDTO= new DictInfoOutputDTO();
        outputDTO.setTotal(page.getTotal());
        outputDTO.setTotalPage(page.getPages());
        outputDTO.setSceneDictlist(sceneDicts);
        return BaseResponse.ok(outputDTO);
    }

    @Override
    public BaseResponse saveDict(SceneCommonInputDTO inputDTO) {
        CommonSceneDict sceneDict = new CommonSceneDict();
        sceneDict.setSceneName(inputDTO.getSceneName());
        sceneDict.setCreateDate(new Date());
        sceneDict.setUpdateDate(new Date());
        sceneDict.setSceneCode(inputDTO.getSceneCode());
        sceneDict.setStatus(inputDTO.isState() ? "1" : "0");
        Integer row = dictDao.insert(sceneDict);
        if(row<1){
            BaseResponse.error("添加规则失败");
        }
        return BaseResponse.ok();
    }



    @Override
    public BaseResponse getDictById(String id) {
        CommonSceneDict sceneDict = dictDao.selectById(id);
        DictBo dictBo = XxmBeanUtils.E2T(sceneDict, DictBo.class);
        return BaseResponse.ok(dictBo);
    }



    @Override
    public BaseResponse editDictById(SceneCommonInputDTO inputDTO) {
        CommonSceneDict sceneDict = XxmBeanUtils.E2T(inputDTO, CommonSceneDict.class);
        sceneDict.setUpdateDate(new Date());
        sceneDict.setStatus(inputDTO.isState()?"1":"0");
        Integer row = dictDao.updateById(sceneDict);
        if(row<1){
            return BaseResponse.error("修改失败");
        }
        return BaseResponse.ok();
    }


    @Override
    public BaseResponse updateDictStatus(String did, boolean flag) {
        CommonSceneDict sceneDict = new CommonSceneDict();
        sceneDict.setId(Long.parseLong(did));
        sceneDict.setStatus(flag?"1":"0");
        sceneDict.setUpdateDate(new Date());
        Integer row = dictDao.updateById(sceneDict);
        if(row == 0 ){
            return BaseResponse.error("修改场景字典失败");
        }
        return BaseResponse.ok();
    }


    /**
     * 删除逻辑删除
     * @param id
     * @return
     */
    @Override
    public BaseResponse deleteDictById(String id) {
        CommonSceneDict sceneDict = new CommonSceneDict();
        sceneDict.setUpdateDate(new Date());
        sceneDict.setId(Long.parseLong(id));
        sceneDict.setStatus("0");
        Integer row = dictDao.updateById(sceneDict);
        if (row > 0) {
            return BaseResponse.ok();
        } else {
            return BaseResponse.error("删除失败");
        }
    }
}
