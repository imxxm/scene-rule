package cn.xxm.scene.service.impl;

import cn.xxm.scene.bo.DictBo;
import cn.xxm.scene.bo.SceneBo;
import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.common.base.ExceptionCodeEum;
import cn.xxm.scene.dao.CommonSceneDictDao;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.dto.output.DictInfoOutputDTO;
import cn.xxm.scene.dto.output.SceneInfoOutputDTO;
import cn.xxm.scene.entity.CommonScene;
import cn.xxm.scene.dao.CommonSceneDao;
import cn.xxm.scene.entity.CommonSceneDict;
import cn.xxm.scene.exception.ExceptionCast;
import cn.xxm.scene.service.CommonSceneService;
import cn.xxm.scene.utils.XxmBeanUtils;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@Service
public class CommonSceneServiceImpl extends ServiceImpl<CommonSceneDao, CommonScene> implements CommonSceneService {



    @Autowired
    private CommonSceneDao sceneDao;
    @Autowired
    private CommonSceneDictDao dictDao;


    @Override
    public BaseResponse selectSceneListByPage(SceneCommonInputDTO inputDTO) {
        Page page = new Page<>(inputDTO.getPagenum(), inputDTO.getPagesize());

        List<SceneBo> sceneBoList = sceneDao.selectSceneListByPage(page, inputDTO);

        SceneInfoOutputDTO outputDTO= new SceneInfoOutputDTO();
        outputDTO.setTotal(page.getTotal());
        outputDTO.setTotalPage(page.getPages());
        outputDTO.setSceneInfoList(sceneBoList);
        return BaseResponse.ok(outputDTO);
    }

    @Override
    public BaseResponse saveScene(SceneCommonInputDTO inputDTO) {
        if(StringUtils.isEmpty(inputDTO.getSceneCode())){
            ExceptionCast.cast(ExceptionCodeEum.USERNAME_OR_PWD_INCORRECT);
        }

        CommonScene commonScene = XxmBeanUtils.E2T(inputDTO, CommonScene.class);
        String sceneCode = inputDTO.getSceneCode();

        EntityWrapper ew = new EntityWrapper();
        ew.eq("scene_code",inputDTO.getSceneCode());
        List<CommonSceneDict> list = dictDao.selectList(ew);
        if(CollectionUtils.isNotEmpty(list)){
            commonScene.setSceneName(list.get(0).getSceneName());
        }


        commonScene.setState(inputDTO.isState()?"1":"0");
        commonScene.setIsTrustDevice(inputDTO.isTrustDevice()?1:0);
        Integer row = sceneDao.insert(commonScene);
        if(row<1){
            BaseResponse.error("添加规则失败");
        }
        return BaseResponse.ok();
    }



    @Override
    public BaseResponse getSceneById(String id) {
        CommonScene commonScene = sceneDao.selectById(id);
        SceneBo sceneBo = XxmBeanUtils.E2T(commonScene, SceneBo.class);
        sceneBo.setTrustDevice(1== commonScene.getIsTrustDevice());
        return BaseResponse.ok(sceneBo);
    }



    @Override
    public BaseResponse editSceneById(SceneCommonInputDTO inputDTO) {
        CommonScene commonScene = XxmBeanUtils.E2T(inputDTO, CommonScene.class);
        commonScene.setIsTrustDevice(inputDTO.isTrustDevice()?1:0);
        commonScene.setState(inputDTO.isState()?"1":"0");
        Integer row = sceneDao.updateById(commonScene);
        if(row<1){
            return BaseResponse.error("修改失败");
        }
        return BaseResponse.ok();
    }


    @Override
    public BaseResponse updateSceneStatus(String sid,String field, boolean flag) {
        CommonScene commonScene = new CommonScene();
        commonScene.setId(Long.parseLong(sid));
        if("state".equals(field)){
            commonScene.setState(flag?"1":"0");
        }
        if("isTrustDevice".equals(field)){
            commonScene.setIsTrustDevice(flag?1:0);
        }
        Integer row = sceneDao.updateById(commonScene);
        if(row == 0 ){
            return BaseResponse.error("修改规则信息失败");
        }
        return BaseResponse.ok();
    }


    /**
     * 删除逻辑删除
     * @param id
     * @return
     */
    @Override
    public BaseResponse deleteSceneById(String id) {
        CommonScene commonScene = new CommonScene();
        commonScene.setId(Long.parseLong(id));
        commonScene.setState("0");
        Integer row = sceneDao.updateById(commonScene);
        if (row > 0) {
            return BaseResponse.ok();
        } else {
            return BaseResponse.error("逻辑删除失败");
        }
    }

    @Override
    public BaseResponse allocateRules(SceneCommonInputDTO inputDTO) {
        StringBuilder ruleCodeBuilder = new StringBuilder();
        List<String> rids = inputDTO.getRids();
        if(CollectionUtils.isNotEmpty(rids)){
            for (int i = 0; i <rids.size() ; i++) {
                ruleCodeBuilder.append(rids.get(i));
                if(i != rids.size()-1){
                    ruleCodeBuilder.append(",");
                }
            }
        }
        CommonScene commonScene = new CommonScene();
        commonScene.setId(Long.parseLong(inputDTO.getId()));
        commonScene.setRuleCodeList(ruleCodeBuilder.toString());
        Integer row = sceneDao.updateById(commonScene);
        if (row > 0) {
            return BaseResponse.ok();
        } else {
            return BaseResponse.error("分配新校验规则失败");
        }
    }
}
