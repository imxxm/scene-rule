package cn.xxm.scene.service.impl;

import cn.xxm.scene.bo.ModelExcel;
import cn.xxm.scene.bo.WhiteExcel;
import cn.xxm.scene.entity.RegistWhite;
import cn.xxm.scene.dao.RegistWhiteDao;
import cn.xxm.scene.service.RegistWhiteService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Xxm123
 * @since 2020-04-04
 */
@Service
public class RegistWhiteServiceImpl extends ServiceImpl<RegistWhiteDao, RegistWhite> implements RegistWhiteService {


    @Autowired
    private RegistWhiteDao registWhiteDao;

    @Override
    public Set<String> selectExistRecordByPhone(Set<ModelExcel> list) {
        Set<String> registWhiteList = registWhiteDao.selectExistRecordByPhone(list);
        return null == registWhiteList ? new HashSet<>() : registWhiteList;
    }

    @Override
    public void insertBatchWhiteList(Set<RegistWhite> registWhiteList) {
        registWhiteDao.insertBatchWhiteList(registWhiteList);
    }



    @Override
    public List<ModelExcel> selectWhiteExcel() {
        return registWhiteDao.selectWhiteExcel();
    }
}
