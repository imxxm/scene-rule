package cn.xxm.scene.service.impl;

import cn.xxm.scene.bo.CheckRuleBo;
import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dao.CommonSceneCheckRuleDao;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.dto.output.RuleInfoOutputDTO;
import cn.xxm.scene.entity.CommonSceneCheckRule;
import cn.xxm.scene.service.CommonSceneCheckRuleService;
import cn.xxm.scene.utils.XxmBeanUtils;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@Service
public class CommonSceneCheckRuleServiceImpl extends ServiceImpl<CommonSceneCheckRuleDao, CommonSceneCheckRule> implements CommonSceneCheckRuleService {

    @Autowired
    private CommonSceneCheckRuleDao checkRuleDao;

    @Override
    public BaseResponse selectRuleListByPage(SceneCommonInputDTO inputDTO) {
        Page page = new Page<>(inputDTO.getPagenum(), inputDTO.getPagesize());

        List<CheckRuleBo> checkRuleBoList = checkRuleDao.selectRuleListByPage(page, inputDTO);

        RuleInfoOutputDTO outputDTO= new RuleInfoOutputDTO();
        outputDTO.setTotal(page.getTotal());
        outputDTO.setTotalPage(page.getPages());
        outputDTO.setRuleList(checkRuleBoList);
        return BaseResponse.ok(outputDTO);
    }

    @Override
    public BaseResponse saveRule(SceneCommonInputDTO inputDTO) {
        CommonSceneCheckRule checkRule = XxmBeanUtils.E2T(inputDTO,CommonSceneCheckRule.class);
        Integer row = checkRuleDao.insert(checkRule);
        if(row<1){
            BaseResponse.error("添加规则失败");
        }
        return BaseResponse.ok();
    }

    @Override
    public BaseResponse getRuleById(String id) {
        CommonSceneCheckRule checkRule = checkRuleDao.selectById(id);
        CheckRuleBo checkSceneBo = XxmBeanUtils.E2T(checkRule, CheckRuleBo.class);
        return BaseResponse.ok(checkSceneBo);
    }


    @Override
    public BaseResponse editRuleById(SceneCommonInputDTO inputDTO) {
        CommonSceneCheckRule checkRule = XxmBeanUtils.E2T(inputDTO, CommonSceneCheckRule.class);
        Integer row = checkRuleDao.updateById(checkRule);
        if(row<1){
            return BaseResponse.error("修改失败");
        }
        return BaseResponse.ok();
    }

    @Override
    public BaseResponse updateRuleStatus(String did, boolean flag) {
        CommonSceneCheckRule checkRule = new CommonSceneCheckRule();
        checkRule.setId(Long.parseLong(did));
        checkRule.setState(flag);
        Integer row = checkRuleDao.updateById(checkRule);
        if(row == 0 ){
            return BaseResponse.error("修改规则");
        }
        return BaseResponse.ok();
    }

    @Override
    public BaseResponse deleteRuleById(String id) {
        CommonSceneCheckRule checkRule = new CommonSceneCheckRule();
        checkRule.setId(Long.parseLong(id));
        checkRule.setState(false);
        Integer row = checkRuleDao.updateById(checkRule);
        if (row > 0) {
            return BaseResponse.ok();
        } else {
            return BaseResponse.error("删除失败");
        }
    }
}
