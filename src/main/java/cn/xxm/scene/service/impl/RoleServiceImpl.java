package cn.xxm.scene.service.impl;

import cn.xxm.scene.entity.Role;
import cn.xxm.scene.dao.RoleDao;
import cn.xxm.scene.service.RoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, Role> implements RoleService {

}
