package cn.xxm.scene.service.impl;

import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.output.MenusOutputDTO;
import cn.xxm.scene.entity.Menu;
import cn.xxm.scene.dao.MenuDao;
import cn.xxm.scene.entity.User;
import cn.xxm.scene.service.MenuService;
import cn.xxm.scene.utils.RedisKeyUtils;
import cn.xxm.scene.utils.RedisUtil;
import cn.xxm.scene.utils.WebHelper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuDao, Menu> implements MenuService {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private MenuDao menuDao;

    @Override
    public BaseResponse getMenus(String token) {
        User user = (User) WebHelper.getRequestAttribute(token);
        // 1. 通过userId  查询menuList
        List<MenusOutputDTO>  menusOutputDTOList =  menuDao.selectMenuListByUserId(user.getId());
        return BaseResponse.ok(menusOutputDTOList);
    }
}
