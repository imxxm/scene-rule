package cn.xxm.scene.service.impl;

import cn.xxm.scene.service.EasyService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class EasyServiceImpl implements EasyService {

    @Override
    public JSONObject testKafka(String fileName) {

        System.out.println("fileName = " + fileName);
        Map map = new HashMap<>();
        map.put("name","xxm");
        map.put("age","17");
        return new JSONObject(map);
    }
}
