package cn.xxm.scene.service.impl;

import cn.xxm.scene.entity.UserToken;
import cn.xxm.scene.dao.UserTokenDao;
import cn.xxm.scene.service.UserTokenService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
@Service
public class UserTokenServiceImpl extends ServiceImpl<UserTokenDao, UserToken> implements UserTokenService {

}
