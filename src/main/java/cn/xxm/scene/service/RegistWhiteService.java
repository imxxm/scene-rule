package cn.xxm.scene.service;

import cn.xxm.scene.bo.ModelExcel;
import cn.xxm.scene.bo.WhiteExcel;
import cn.xxm.scene.entity.RegistWhite;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxm123
 * @since 2020-04-04
 */
public interface RegistWhiteService extends IService<RegistWhite> {

    Set<String> selectExistRecordByPhone(Set<ModelExcel> list);

    void insertBatchWhiteList(Set<RegistWhite> registWhiteList);

    List<ModelExcel> selectWhiteExcel();
}
