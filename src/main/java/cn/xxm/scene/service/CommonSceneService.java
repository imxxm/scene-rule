package cn.xxm.scene.service;

import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.entity.CommonScene;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface CommonSceneService extends IService<CommonScene> {

    BaseResponse selectSceneListByPage(SceneCommonInputDTO inputDTO);

    BaseResponse saveScene(SceneCommonInputDTO inputDTO);

    BaseResponse getSceneById(String id);

    BaseResponse editSceneById(SceneCommonInputDTO inputDTO);

    BaseResponse updateSceneStatus(String did, String field,boolean flag);

    BaseResponse deleteSceneById(String id);

    BaseResponse allocateRules(SceneCommonInputDTO inputDTO);
}
