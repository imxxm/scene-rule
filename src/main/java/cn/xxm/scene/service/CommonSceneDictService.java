package cn.xxm.scene.service;

import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.entity.CommonSceneDict;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface CommonSceneDictService extends IService<CommonSceneDict> {

    BaseResponse selectDictListByPage(SceneCommonInputDTO inputDTO);

    BaseResponse saveDict(SceneCommonInputDTO inputDTO);

    BaseResponse getDictById(String id);

    BaseResponse editDictById(SceneCommonInputDTO id);

    BaseResponse updateDictStatus(String did, boolean flag);

    BaseResponse deleteDictById(String id);
}
