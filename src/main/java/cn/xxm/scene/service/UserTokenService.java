package cn.xxm.scene.service;

import cn.xxm.scene.entity.UserToken;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface UserTokenService extends IService<UserToken> {

}
