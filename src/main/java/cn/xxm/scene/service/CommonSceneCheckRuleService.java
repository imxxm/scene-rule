package cn.xxm.scene.service;

import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.SceneCommonInputDTO;
import cn.xxm.scene.entity.CommonSceneCheckRule;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface CommonSceneCheckRuleService extends IService<CommonSceneCheckRule> {


    BaseResponse selectRuleListByPage(SceneCommonInputDTO inputDTO);

    BaseResponse saveRule(SceneCommonInputDTO inputDTO);

    BaseResponse getRuleById(String id);

    BaseResponse editRuleById(SceneCommonInputDTO inputDTO);

    BaseResponse updateRuleStatus(String did, boolean flag);

    BaseResponse deleteRuleById(String id);
}
