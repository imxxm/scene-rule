package cn.xxm.scene.service;

import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.dto.input.LoginInputDTO;
import cn.xxm.scene.dto.input.RoleInputDTO;
import cn.xxm.scene.dto.input.UserInfoInputDTO;
import cn.xxm.scene.entity.User;
import cn.xxm.scene.validate.response.FSResponseModel;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface UserService extends IService<User> {

    FSResponseModel login(LoginInputDTO inputDTO);

    BaseResponse getUserList(UserInfoInputDTO inputDTO);

    BaseResponse getUserById(String id);

    BaseResponse updateUserById(UserInfoInputDTO infoInputDTO);

    BaseResponse deleteUserById(String id);

    BaseResponse updateRoleById(RoleInputDTO roleInputDTO);

    BaseResponse saveUser(UserInfoInputDTO inputDTO);

    BaseResponse updateUserStatus(String uid, boolean flag);

}
