package cn.xxm.scene.service;

import cn.xxm.scene.common.base.BaseResponse;
import cn.xxm.scene.entity.Menu;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxm123
 * @since 2020-02-22
 */
public interface MenuService extends IService<Menu> {

    BaseResponse getMenus(String token);
}
