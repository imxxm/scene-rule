package cn.xxm.scene.validate.exception;

public class ValidateException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ValidateException(String msg){
		super(msg);
	}
	
	public ValidateException(Throwable throwable){
		super(throwable);
	}
	
	public ValidateException(String msg,Throwable throwable){
		super(msg,throwable);
	}
}
