package cn.xxm.scene.validate.processor;

import cn.xxm.scene.validate.response.ResponseInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ValidateProcessor<T extends Enum<?> & ResponseInfo> {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	private T responseInfo;

	public abstract boolean validate(Object value);

	public T getResponseInfo() {
		return responseInfo;
	}

	public  void setResponseInfo(T responseInfo) {
		this.responseInfo = responseInfo;
	}
	
}
