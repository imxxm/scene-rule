package cn.xxm.scene.validate.processor;




import cn.xxm.scene.validate.annotation.VRegex;
import cn.xxm.scene.validate.exception.ValidateException;
import cn.xxm.scene.validate.response.ResponseInfo;

import java.util.regex.Pattern;

@SuppressWarnings("rawtypes")
public class RegexProcessor extends ValidateProcessor {
	private Pattern pattern;
	
	@SuppressWarnings("unchecked")
	public RegexProcessor(VRegex vRegex){
		Class<Enum> enumClass = (Class<Enum>)vRegex.enumClass();
		Enum enumInstance = Enum.valueOf(enumClass, vRegex.enumName());
		if(! (enumInstance instanceof ResponseInfo)) {//枚举类型没实现ResponseInfo则抛异常
			throw new ValidateException("枚举类型<"+enumClass+">未实现接口<"+ResponseInfo.class+">")  ;
		}
		super.setResponseInfo(enumInstance);
		pattern = Pattern.compile(vRegex.regex());
	}
	
	@Override
	public boolean validate(Object value) {
		return pattern.matcher(value.toString()).matches();
	}
}
