package cn.xxm.scene.validate.processor;


import cn.xxm.scene.validate.annotation.VNotNull;
import cn.xxm.scene.validate.annotation.VNull;
import cn.xxm.scene.validate.exception.ValidateException;
import cn.xxm.scene.validate.response.ResponseInfo;

@SuppressWarnings("rawtypes")
public class NullProcessor extends ValidateProcessor {
	private boolean notNull = true;

	@SuppressWarnings("unchecked")
	public NullProcessor(VNull vNull){
		Class<Enum> enumClass = (Class<Enum>)vNull.enumClass();
		Enum enumInstance = Enum.valueOf(enumClass, vNull.enumName());
		if(! (enumInstance instanceof ResponseInfo)) {//枚举类型没实现ResponseInfo则抛异常
			throw new ValidateException("枚举类型<"+enumClass+">未实现接口<"+ResponseInfo.class+">")  ;
		}
		super.setResponseInfo(enumInstance);
		this.notNull = false;
	}
	
	@SuppressWarnings("unchecked")
	public NullProcessor(VNotNull vNotNull){
		Class<Enum> enumClass = (Class<Enum>)vNotNull.enumClass();
		Enum enumInstance = Enum.valueOf(enumClass, vNotNull.enumName());
		if(! (enumInstance instanceof ResponseInfo)) {//枚举类型没实现ResponseInfo则抛异常
			throw new ValidateException("枚举类型<"+enumClass+">未实现接口<"+ResponseInfo.class+">")  ;
		}
		super.setResponseInfo(enumInstance);
		this.notNull = true;
	}
	
	@Override
	public boolean validate(Object value){
		return notNull ^ validateNull(value);
	}
	
	public boolean validateNull(Object value){
		return value == null;
	}

	public boolean isNotNull(){
		return notNull;
	}
	
	public void setNotNull(boolean notNull){
		this.notNull = notNull;
	}
	
	public boolean isRequiredNull(){
		return !notNull;
	}
	
	public void setRequiredNull(boolean requiredNull){
		notNull = !requiredNull;
	}
	
}
