package cn.xxm.scene.validate.processor;




import cn.xxm.scene.validate.annotation.VEnum;
import cn.xxm.scene.validate.exception.ValidateException;
import cn.xxm.scene.validate.response.ResponseInfo;

import java.util.Arrays;


@SuppressWarnings("rawtypes")
public class EnumProcessor extends ValidateProcessor {
	private String [] enumNames;
	
	@SuppressWarnings("unchecked")
	public EnumProcessor(VEnum vEnum){
		Class<Enum> enumClass = (Class<Enum>)vEnum.enumClass();
		Enum enumInstance = Enum.valueOf(enumClass, vEnum.enumName());
		if(! (enumInstance instanceof ResponseInfo)) {//枚举类型没实现ResponseInfo则抛异常
			throw new ValidateException("枚举类型<"+enumClass+">未实现接口<"+ResponseInfo.class+">")  ;
		}
		super.setResponseInfo(enumInstance);
		this.enumNames = vEnum.enumNames();
	}

	@Override
	public boolean validate(Object value) {
		int idx = Arrays.binarySearch(enumNames, value.toString());
		return idx >= 0;
	}
}
