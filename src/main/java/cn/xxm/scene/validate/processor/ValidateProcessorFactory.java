package cn.xxm.scene.validate.processor;



import cn.xxm.scene.validate.annotation.*;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;


@SuppressWarnings("rawtypes")
public class ValidateProcessorFactory {
	public static Map<Annotation, ValidateProcessor> validateProcessorCache = new HashMap<Annotation, ValidateProcessor>();
	
	public static ValidateProcessor getValidateProcessor(Annotation annotation){
		ValidateProcessor processor = validateProcessorCache.get(annotation);
		if(processor == null){
			if(annotation instanceof VNull){
				processor = new NullProcessor((VNull)annotation);
			} else if(annotation instanceof VNotNull){
				processor = new NullProcessor((VNotNull)annotation);
			} else if(annotation instanceof VEmpty){
				processor = new EmptyProcessor((VEmpty)annotation);
			} else if(annotation instanceof VNotEmpty){
				processor = new EmptyProcessor((VNotEmpty)annotation);
			} else if(annotation instanceof VNumber){
				processor = new NumberProcessor((VNumber)annotation);
			} else if(annotation instanceof VRegex){
				processor = new RegexProcessor((VRegex)annotation);
			} else if(annotation instanceof VLength){
				processor = new LengthProcessor((VLength)annotation);
			} else if(annotation instanceof VEnum){
				processor = new EnumProcessor((VEnum)annotation);
			} else if(annotation instanceof VDate){
				processor = new DateProcessor((VDate)annotation);
			}
			validateProcessorCache.put(annotation, processor);
		}
		return processor;
	}
	
}
