package cn.xxm.scene.validate.executor;


import cn.xxm.scene.validate.response.ValidatorResult;

import java.lang.annotation.Annotation;

/**
 * @author xxm
 * @create 2020-11-19 15:26
 */
public interface ValidatorExecutor {

    ValidatorResult<?> validate(Annotation annotation, Object value);

}
