package cn.xxm.scene.validate.executor;



import cn.xxm.scene.validate.response.CommonResponseInfo;
import cn.xxm.scene.validate.response.ValidatorResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.reflect.FastMethod;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractValidatorExecutor implements ValidatorExecutor{
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	protected Map<Class<?>, ValidatorExecuterDelegate> validateCache = new HashMap<Class<?>, ValidatorExecuterDelegate>();
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ValidatorResult<?> validate(Annotation annotation, Object value) {
		ValidatorResult result = null;
		try {
			result = doValidate(value);
		} catch (Exception e) {
			logger.error("param validate error:{}",e);
			result.setValid(false);
			result.setResponseInfo(CommonResponseInfo.FAILED);
		}
		return result;
	}

	protected abstract ValidatorResult<?> doValidate(Object value) throws InvocationTargetException;



	protected ValidatorExecuterDelegate getValidatorExecuterCache(Class<?> clazz){
		ValidatorExecuterDelegate validatorExecuterCache = validateCache.get(clazz);
		if(validatorExecuterCache == null){
			validatorExecuterCache = new ValidatorExecuterDelegate(clazz);
			validateCache.put(clazz, validatorExecuterCache);
		}
		return validatorExecuterCache;
	}
	
}