package cn.xxm.scene.validate.annotation;




import cn.xxm.scene.validate.response.CommonResponseInfo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER,ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface VNumber {
	double min() default Double.MIN_VALUE;
	double max() default Double.MAX_VALUE;
	int precision() default -1;
	@SuppressWarnings("rawtypes")
	Class<? extends Enum> enumClass() default CommonResponseInfo.class;
	String enumName() default "PARAM_ERROR";
}
