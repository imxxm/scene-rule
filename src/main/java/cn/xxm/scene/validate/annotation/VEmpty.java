package cn.xxm.scene.validate.annotation;




import cn.xxm.scene.validate.response.CommonResponseInfo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER,ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface VEmpty {
	boolean blankStringIsEmpty() default true;//空白字符串是否视为空字符串
	@SuppressWarnings("rawtypes")
	Class<? extends Enum> enumClass() default CommonResponseInfo.class;
	String enumName() default "PARAM_ERROR";
}
