package cn.xxm.scene.validate.annotation;




import cn.xxm.scene.validate.response.CommonResponseInfo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER,ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface VNotEmpty {
	/**
	 * 空白字符串是否视为空字符串，此选项仅对字符串生效，默认为true<BR/>
	 * null视为空字符串，"null"不视为空字符串
	 * true-空白字符串视为空字符串，如"&nbsp;&nbsp;&nbsp;"也认为是空字符串<BR/>
	 * false-空白字符串不视为空字符串，仅""为空字符串<BR/>
	 */
	boolean blankStringIsEmpty() default true;
	@SuppressWarnings("rawtypes")
	Class<? extends Enum> enumClass() default CommonResponseInfo.class;
	String enumName() default "PARAM_ERROR";
}
