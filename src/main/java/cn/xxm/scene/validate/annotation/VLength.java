package cn.xxm.scene.validate.annotation;




import cn.xxm.scene.validate.response.CommonResponseInfo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER,ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface VLength {
	int min() default -1;
	int max() default -1;
	boolean byteUnit() default false;//是否以字节计算长度
	String charset() default "";
	@SuppressWarnings("rawtypes")
	Class<? extends Enum> enumClass() default CommonResponseInfo.class;
	String enumName() default "PARAM_ERROR";
}
