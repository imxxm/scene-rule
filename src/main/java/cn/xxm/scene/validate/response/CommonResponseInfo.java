package cn.xxm.scene.validate.response;

/**
 * 公共响应信息
 */
public enum CommonResponseInfo implements ResponseInfo {
	/** 0000000-success：交易成功 **/
	SUCCESS("000000", "success"),
	/** 0000001-failed：系统繁忙，请稍后再试 **/
	FAILED("000001", "failed"),
	/** 000002-sessionTimeout：会话超时，请重新登录 **/
	SESSION_TIMEOUT("000002", "sessionTimeout"),
	/** 000003-paramError：参数错误 **/
	PARAM_ERROR("000003", "paramError"),
	/** 000004-otherLogin：您的账号在其他地点登录，您被强制下线 **/
	OTHER_LOGIN("000004", "otherLogin"),
	/** 000005-notLogin：您的账号尚未登录，请先登录 **/
	NOT_LOGIN("000005", "notLogin"),
	/** 000006-requestError：请求异常 **/
	REQUEST_ERROR("000006", "requestError"),
	/** 000007-requestStoped：请求接口已关闭，无法访问 **/
	REQUEST_STOPED("000007", "requestStoped"),
	/** 000008-requestAuthFail：没有权限访问该接口 **/
	REQUEST_AUTH_FAIL("000008", "requestAuthFail"),
	/** 000009-requestException：服务异常 **/
	REQUEST_EXCEPTION("000009", "requestException"),
	/** 000010-branschRespError：分支流程异常 **/
	BRANCH_RESP_ERROR("000010", "branschRespError"),
	/** 000011-overtimeRequest：过期的请求 **/
	OVERTIME_REQUEST("000011", "overtimeRequest"),
	/** 000012-failAccountSercurity：账户安全验证失败 **/
	FAIL_ACCOUNT_SERCURITY("000012", "failAccountSercurity"),
	/** 000013-newVersion：发现新版本，请更新体验 **/
	NEW_VERSION("000013", "newVersion"),
	/** 000014-conflictVersion：版本冲突 **/
	CONFLICT_VERSION("000014", "conflictVersion"),
	/** 000015-ipNumExceedLimit：该IP注册超过最大次数 **/
	IP_NUM_EXCEED_LIMIT("000015", "ipNumExceedLimit"),
	/** 000016-duplicateRequest：重复的请求 **/
	DUPLICATE_REQUEST("000016", "duplicateRequest"),
	/** 000017-rejectRequest：请求被拒绝 **/
	REJECT_REQUEST("000017", "rejectRequest"),
	/** 000018-unknownDevices：设备登录异常 **/
	UNKNOWN_DEVICES("000018", "unknownDevices"),
	/** 000019-securityValidateError：接口安全验证失败 **/
	SECURITY_VALIDATE_ERROR("000019", "securityValidateError"),
	/** 000020-unknownError：未知错误 **/
	UNKNOWN_ERROR("000020", "unknownError"),
	/** 000021-accountRisk：账户存在风险 **/
	ACCOUNT_RISK("000021", "accountRisk"),
	/** 000022-deviceNumExceedLimit：该设备注册超过最大次数 **/
	DEVICE_NUM_EXCEED_LIMIT("000022", "deviceNumExceedLimit"),
	/** 000023-paramCheckFail：参数验证失败 **/
	PARAM_CHECK_FAIL("000023", "paramCheckFail"),
	/** 000024-publicKeyNull：公钥为空 **/
	PUBLICKEY_NULL("000024", "publicKeyNull"),
	/** 000025-publicKeyNumberErr：公钥数量错误 **/
	PUBLICKEY_NUMBER_ERR("000025", "publicKeyNumberErr"),
	/** 000026-thirdServiceConfigNotExists：第三方接口服务配置不存在 **/
	THIRD_SERVICE_CONFIG_NOT_EXISTS("000026", "thirdServiceConfigNotExists"),
	/** 000027-invokeServiceException：调用服务异常 **/
	INVOKE_SERVICE_EXCEPTION("000027", "invokeServiceException"),
	/** 000028-responseException：返回处理异常 **/
	RESPONSE_EXCEPTION("000028", "responseException"),
	/** 0-paicSuccess：PAIC交易成功 **/
	PAIC_SUCCESS("0", "paicSuccess"),
	/** 300030-failGetUserInfo：获取用户信息失败 **/
	FAIL_GET_USERINFO("300030", "failGetUserInfo"),
	/** 000029-no_permission_for_id_card_todo：没有权000028限访问该表 **/
	NO_PERMISSION_FOR_ID_CARD_TODO("000029", "no_permission_for_id_card_todo"),
	/** 0-paicSuccess：PAIC交易成功 **/
	SSO_SUCCESS("000000", "ssoSuccess"),
	/** 000030-dbCheckRuleConfigError：db检查配置错误 **/
	DB_CHECK_RULE_CONFIG_ERROR("000030", "dbCheckRuleConfigError"),
	/** 000031-checkRuleSceneCodeError：授权检查规则错误 **/
	CHECK_RULE_SCENE_CODE_ERROR("000031", "checkRuleSceneCodeError"),
	/** 000032-queryEmpty：查询记录为空 **/
	QUERY_EMPTY("000032", "queryEmpty"),
	/** 200060-：frontSmsVerifyOtpFailed:短信验证码验证失败 **/
	Front_SMS_VERIFY_OTP_FAILED("200060","frontSmsVerifyOtpFailed"),
	/** 200061-：getServcieFromApplicationFail:从spring容器中获取service失败 **/
	GET_SERVCIE_FROM_APPLICATION_FAIL("200061","getServcieFromApplicationFail"),
	/** 200062-：amountOutOfRange:金额超过限额 **/
	AMOUNT_OUT_OF_RANGE("200062","amountOutOfRange"),
	/** 200063-：transactionStatusIsExpired:交易状态已过时 **/
	TRANSACTION_STATUS_IS_EXPIRED("200063","transactionStatusIsExpired"),
	/** 200064-：decodeFileException:文件解密异常 **/
	DECODE_FILE_EXCEPTION("200064","decodeFileException"),
	/**  抱歉，由於多次輸入錯誤一次性密碼，你的一次性密碼將被鎖定30分鐘。  otherSmsVerifyErrTooMuch**/
	OTHER_SMS_VERIFY_ERR_TOO_MUCH("010004","otherSmsVerifyErrTooMuch"),
	/** 010006-：otherResetPwdErrorTimesLimit: 一次性密碼無效 **/
	OTHER_RESET_PWD_ERROR_TIMES_LIMIT("010006","otherResetPwdErrorTimesLimit"),
	/** 010007-：targetServiceIsTemporarilyUnavailable: 目标服务暂不可用 **/
	TARGET_SERVICE_IS_TEMPORARILY_UNAVAILABLE("010007","targetServiceIsTemporarilyUnavailable"),
	/** 010008-：gatewayOccurException: gateway异常 **/
	GATEWAY_OCCUR_EXCEPTION("010008","gatewayOccurException"),
	/** 010009-：bcustomerRequestDataNotEmpty: B门户接口,data参数不能为空 **/
	BCUSTOMER_REQUEST_DATA_NOT_EMPTY("010009","bcustomerRequestDataNotEmpty"),
	/** 010010-：currentUserIsNotWhiteUser: 当前用户不是白名单用户 **/
	CURRENT_USER_IS_NOT_WHITE_USER("010010","currentUserIsNotWhiteUser"),
	/** 010011-：mobileIllegal:邮箱号格式不正确 **/
	EMAIL_ILLEGAL("010011","emailIllegal"),
	/** 010012-：channelNoIsNotExist:渠道号不存在 **/
	CHANNEL_NO_IS_NOT_EXIST("010012","channelNoIsNotExist"),
	/** 010013-：statusValueIsOutOfMemorry:状态值不在设定范围内 **/
	STATUS_VALUE_IS_OUT_OF_MEMORRY("010013","状态值不在设定范围内"),
	/** 010014-：getGifCodeError:获取图片验证码异常 **/
	GET_GIF_CODE_ERROR("010014","getGifCodeError"),
	/** 010015-checkConditonNotPass：准入条件检查不通过 **/
	CHECK_CONDITION_NOT_PASS("010015", "checkConditonNotPass"),
	/** 010016-：failToQueryParamValue:查询参数失败 **/
	FAIL_TO_QUERY_PARAM_VALUE("010016","failToQueryParamValue"),
	/** 010017-：buildNumberError:您必須更新到新版本才能使用此應用程式。 **/
	BUILD_NUMBER_ERROR("010017","buildNumberError");

	private String code;
	private String msg;

	private CommonResponseInfo(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getMsg() {
		return msg;
	}

	@Override
	public String toString() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("{code:");
		strBuf.append(getCode());
		strBuf.append(",msg:");
		strBuf.append(getI18nMsg());
		strBuf.append("}");
		return strBuf.toString();
	}

}
