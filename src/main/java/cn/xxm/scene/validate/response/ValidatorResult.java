package cn.xxm.scene.validate.response;


/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-11-23 17:39
 **/
public class ValidatorResult <E extends Enum<?> & ResponseInfo > {
    private boolean valid;
    private E responseInfo;

    public ValidatorResult() {
        this.valid = true;
        this.responseInfo =(E) CommonResponseInfo.SUCCESS;
    }


    public ValidatorResult(boolean valid, E responseInfo) {
        this.valid = valid;
        this.responseInfo = responseInfo;
    }


    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public E getResponseInfo() {
        return responseInfo;
    }

    public void setResponseInfo(E responseInfo) {
        this.responseInfo = responseInfo;
    }
}
