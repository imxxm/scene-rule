package cn.xxm.scene.validate.response;

import cn.xxm.scene.common.base.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-11-23 17:17
 **/
public class FSResponseModel<T> implements ResponseInfo, Serializable {
    private static final long serialVersionUID = -2207828976535484464L;

    // 状态码
    private String code;
    // 业务提示语
    private String msg;
    // 数据对象
    private T data;

    /**
     * 无参构造,默认交易成功的响应消息
     */
    public FSResponseModel() {
        setCodeAndMsg(CommonResponseInfo.SUCCESS);
    }

    public static <T> FSResponseModel ok(T data) {
        FSResponseModel<T> commonResponse = new FSResponseModel<>(CommonResponseInfo.SUCCESS);
        commonResponse.setData(data);
        return commonResponse;
    }


    public <E extends Enum<?> & ResponseInfo > FSResponseModel(E responseInfo){
        setCodeAndMsg(responseInfo);
    }

    public <E extends Enum<?> & ResponseInfo > FSResponseModel(E responseInfo,T data){
        setCodeAndMsg(responseInfo,data);
        this.data = data;
    }





    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public <E extends Enum<?> & ResponseInfo > void setCodeAndMsg(E responseInfo){
        this.code = responseInfo.getCode();
        this.msg = getI18nMsg(responseInfo);
    }

    public <E extends Enum<?> & ResponseInfo > void setCodeAndMsg(E responseInfo,T data){
        this.code = responseInfo.getCode();
        this.msg = getI18nMsg(responseInfo);
        this.data = data;
    }




    public static String getI18nMsg(ResponseInfo responseInfo){
        String oriMsg = responseInfo.getMsg();
        String i18nKey = responseInfo.getClass().getSimpleName().replace("ResponseInfo","").toLowerCase()+"." +oriMsg;
        //String i18nMsg = I18nUtil.getI18nValue(i18nKey);
        //return i18nMsg == null ? oriMsg : i18nMsg;
        return oriMsg;

    }









}
