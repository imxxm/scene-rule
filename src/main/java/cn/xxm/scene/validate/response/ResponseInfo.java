package cn.xxm.scene.validate.response;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 响应信息顶层接口
 * 响应码6位数字,规则为模块编码(\d{2})+接口编码(\d{2})+响应信息编码(\d{2})
 */
public interface ResponseInfo {
	/** 公共模块 **/
	public static String COMMON_MODULE = "00";
	/** 基础模块 **/
	public static String BASE_MODULE = "01";
	/** 用户模块 **/
	public static String USER_MODULE = "02";
	/** 关联方对接(接出)模块 **/
	public static String OUT_MODULE = "03";
	
	/**	获取响应码 **/
	public abstract String getCode();
	/** 获取响应信息 **/
	public abstract String getMsg();

	/**
	 * i18n默认实现
	 *
	 * @return  i18n信息
	 */
	@JsonIgnore
	@JSONField(serialize = false)
	default String getI18nMsg() {
		String oriMsg = getMsg();
		String i18nKey = this.getClass().getSimpleName().replace("ResponseInfo", "").toLowerCase() + "."
				+ oriMsg;
		//String msg = I18nUtil.getI18nValue(i18nKey);
		//return msg == null ? oriMsg : msg;
        return oriMsg;
	}
}
