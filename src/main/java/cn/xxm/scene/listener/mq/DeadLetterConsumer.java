package cn.xxm.scene.listener.mq;

import cn.xxm.scene.config.rabbitmq.MQMessageDTO;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-08-30 15:23
 **/

@Slf4j
@Component
@ConditionalOnProperty(name = {"ff.mb.rabbit.dead.letter.exchange.switch"}, havingValue = "true", matchIfMissing = false)
public class DeadLetterConsumer{

    @RabbitListener(queues = "${ff.mb.rabbit.receive.jetco.queueName}")
    public void consumeMessage(MQMessageDTO mqMessageDTO, Message message, Channel channel) throws Exception{
        long start = System.nanoTime();
        try {
            log.info(">>>jectBo:{}", JSONObject.toJSONString(mqMessageDTO));
            String custNo = mqMessageDTO.getCustNo();
            if(StringUtils.equals(custNo,"9527")){
                channel.basicAck(message.getMessageProperties().getDeliveryTag(),false); // 成功消费
            }else{
                channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,true);// 消息重试
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        long end = System.nanoTime();
        log.info(">>>consume msg cost:【{}ns】",(end - start));
    }

}
