package cn.xxm.scene.dto.input;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-23 11:38
 **/
@Data
@ToString
public class RoleInputDTO implements Serializable {
    private static final long serialVersionUID = 5887683813008687026L;

    /**
     * roleId
     */
    private String rid;

    /**
     * userId
     */
    private String uid;

}
