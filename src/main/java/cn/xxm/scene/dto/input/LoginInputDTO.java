package cn.xxm.scene.dto.input;

import cn.xxm.scene.validate.annotation.VNotEmpty;
import cn.xxm.scene.validate.response.CommonResponseInfo;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 14:45
 **/
@Data
@ToString
public class LoginInputDTO implements Serializable {
    private static final long serialVersionUID = 7503657603871903291L;

    @VNotEmpty(enumClass = CommonResponseInfo.class,enumName = "FAIL_TO_QUERY_PARAM_VALUE")
    private String username;
    @VNotEmpty(enumClass = CommonResponseInfo.class,enumName = "BUILD_NUMBER_ERROR")
    private String password;
    /**
     * 登录终端
     * 1:ios
     * 2:android
     * 3:wap
     * 4:web
     */
    private String clientType = "4";
    /**
     * 登录方式:1:账密,2:指纹,3:人脸
     */
    private String loginType = "1";


}
