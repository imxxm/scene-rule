package cn.xxm.scene.dto.input;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 14:45
 **/
@Data
@ToString
public class UserInfoInputDTO implements Serializable {
    private static final long serialVersionUID = 7503657603871903291L;

    private String id;
    private String query;
    private String email;
    private String mobile;
    private boolean state;
    private String password;
    private String username;

    private int pagenum = 1;
    private int pagesize = 10;
}
