package cn.xxm.scene.dto.input;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-23 14:40
 **/
@Data
@ToString
public class SceneCommonInputDTO implements Serializable {
    private static final long serialVersionUID = -181592506548214025L;

    private String id;
    private String query;
    private String sceneName;
    private String sceneCode;
    private boolean state;

    private String checkRuleKey;
    private String checkRuleVaule;
    private String checkRuleDesc;
    private String remark;
    private String targetHandler;


    private String sceneDesc;
    private boolean isTrustDevice;
    private String userFlag;
    private String userType;

    private BigDecimal amountUpperLimmit;
    private BigDecimal amountLowerLimmit;
    private String loginType;
    private String invokeRequestUri;
    private String ruleCodeList;


    private List<String> rids;
    private int pagenum = 1;
    private int pagesize = 10;

}
