package cn.xxm.scene.dto.output;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 14:47
 **/
@Data
@ToString
public class LoginOutputDTO implements Serializable {


    private String token;
}
