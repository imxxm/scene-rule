package cn.xxm.scene.dto.output;

import cn.xxm.scene.bo.SceneBo;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 14:45
 **/
@Data
@ToString
public class SceneInfoOutputDTO implements Serializable {
    private static final long serialVersionUID = 7503657603871903291L;


    private int total;
    private int totalPage;

    private List<SceneBo> sceneInfoList;
}
