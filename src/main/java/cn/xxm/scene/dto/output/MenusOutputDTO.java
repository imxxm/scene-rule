package cn.xxm.scene.dto.output;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 14:47
 **/
@Data
@ToString
public class MenusOutputDTO implements Serializable {

    private String id;
    private String authName;
    private String menuDesc;
    private String menuDeep;
    private String isLeaf;
    private String parentId;
    private String path;

    private List<MenusOutputDTO> children;
}
