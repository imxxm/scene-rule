package cn.xxm.scene.bo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 23:30
 **/
@Data
@ToString
public class SceneBo implements Serializable {

    private static final long serialVersionUID = -643964711236154027L;

    private String id;
    private String sceneName;
    private String sceneCode;
    private String sceneDesc;
    @JSONField(name="isTrustDevice")
    private boolean isTrustDevice;
    private String amountLowerLimmit;
    private String amountUpperLimmit;
    private String invokeRequestUri;
    private String userFlag;
    private String userType;
    private String ruleCodeList;
    //@JsonProperty("state")
    //@JSONField(name="state")
    private boolean state;

    private List<CheckRuleBo> checkRuleList;
}
