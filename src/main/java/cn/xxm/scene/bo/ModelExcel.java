package cn.xxm.scene.bo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.google.common.base.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModelExcel  implements Serializable {
    @ExcelIgnore
    private static final long serialVersionUID = -1923473597486297493L;


    /**
     * value: 表头名称
     * index: 列的号,0代表第一列
     */
    @ExcelProperty(value = "appVersion",index = 0)
    @ColumnWidth(12)
    private String appVersion;
 
    @ExcelProperty(value = "mbNumber",index = 1)
    @ColumnWidth(18)
    private String mbNumber;
 
    @ExcelProperty(value = "email",index = 2)
    @ColumnWidth(22)
    private String email;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelExcel that = (ModelExcel) o;
        return Objects.equal(mbNumber, that.mbNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mbNumber);
    }
}
