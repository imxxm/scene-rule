package cn.xxm.scene.bo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 23:30
 **/
@Data
@ToString
public class CheckRuleBo implements Serializable {

    private static final long serialVersionUID = -643964711236154027L;

    private String id;
    private String checkRuleKey;
    private String checkRuleVaule;
    private String checkRuleDesc;
    private String remark;
    private String targetHandler;
    private boolean state;

}
