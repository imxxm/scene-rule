package cn.xxm.scene.bo;

import lombok.Data;
import org.springframework.data.annotation.Id;
//
import java.io.Serializable;

@Data
public class WhiteExcel implements Serializable {
    private static final long serialVersionUID = -1923473597486297493L;

    @Id
    //@Excel(name = "appVersion")
    private int appVersion;

    //@Excel(name = "mbNumber")
    private String mbNumber;

    //@Excel(name = "email")
    private String email;

}
