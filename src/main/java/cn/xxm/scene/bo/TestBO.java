package cn.xxm.scene.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-09-12 00:12
 **/
@Data
public class TestBO implements Serializable {
    private static final long serialVersionUID = -6920508019623464988L;

    private String builderNumber ; //

    private String version ; //


}
