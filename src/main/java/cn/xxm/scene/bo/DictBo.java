package cn.xxm.scene.bo;

import cn.xxm.scene.entity.Role;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 23:30
 **/
@Data
@ToString
public class DictBo implements Serializable {

    private static final long serialVersionUID = -643964711236154027L;

    private String id;
    private String sceneName;
    private String sceneCode;
    //@JsonProperty("state")
    @JSONField(name="state")
    private boolean status;
    private Date createDate;
}
