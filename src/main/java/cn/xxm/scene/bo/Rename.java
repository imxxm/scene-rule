package cn.xxm.scene.bo;

import cn.xxm.scene.utils.StringUtil;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;

/**
 * 重命名规则类
 * @author jack
 */
class ReplacementChain{

    // 去除小括号及其内容 正则表达式
    public static String smallBracketsRegex = "\\(.*?\\)|\\)|（.*?）|）";

    // 去除中括号及其内容 正则表达式
    public static String midBracketsRegex = "\\[.*?\\]";

    // 去除大括号及其内容 正则表达式
    public static String bigBracketsRegex = "\\{.*?\\}";

    // 去除strong括号及其内容 正则表达式  【】
    public static String strongBracketsRegex = "\\【.*?\\】";

    private Map<String,List> map;

    private List<Map<String, String>> replaceList;

    private List<String> prefixList;

    private List<String> suffixList;

    private List<String> regexReplaceList;

    public ReplacementChain() {
        this.map = new HashMap<>();
    }

    public Map<String,List> getMap() {
        return map;
    }

    // 添加新的替换规则(字符串替换)
    public ReplacementChain addRegulation(List<Map<String, String>> replaceList,List<String> prefixList,List<String> suffixList,List<String> regexReplaceList){

        this.map.put("replaceList", replaceList);
        this.map.put("prefixList", prefixList);
        this.map.put("suffixList", suffixList);
        this.map.put("regexReplaceList", regexReplaceList);
        return this;
    }

}

/**
 * 重命名类
 * @author Jack
 */
public class Rename {

    /**
     * 批量重命名
     * @param path
     * @param replacementChain
     */
    public static void multiRename(String path,ReplacementChain replacementChain){
        File file = new File(path);
        boolean isDirectory = file.isDirectory();

        /** 如果不是文件夹，就返回* */
        if(!isDirectory){
            System.out.println(path + "不是一个文件夹！");
            return;
        }

        String[] files = file.list();
        File f = null;
        String filename = "";
        String oldFileName = ""; //之前的名字
        /** 循环遍历所有文件* */
        for(String fileName : files){
            oldFileName = fileName;
            //Map<String, Object> map = replacementChain.getMap();
            Map<String,List> map = replacementChain.getMap();
            for (Entry<String, List> entry : map.entrySet()) {
                String key = entry.getKey();
                List value = entry.getValue();

                if(null == value) continue;

                if("replaceList".equals(key)){
                    List<Map<String,String>> replaceList =  value;
                    for (Map<String, String> replaceMap : replaceList) {
                        for (Entry<String, String> replaceEntry : replaceMap.entrySet()) {
                            String preFileName = fileName;
                            fileName = fileName.replace(replaceEntry.getKey(), replaceEntry.getValue());
                            if(!StringUtils.equals(preFileName,fileName)){
                                System.out.println("path:"+path+" fileName : "+preFileName+" → "+fileName+" ");
                            }
                        }
                    }
                }

                if("prefixList".equals(key)){
                    List<String> prefixList = (List<String>) value;
                    for (String prefixStr : prefixList) {
                        String preFileName = fileName;
                        fileName = prefixStr + fileName;

                        if(!StringUtils.equals(preFileName,fileName)){
                            System.out.println("path:"+path+" fileName : "+preFileName+" → "+fileName+" ");
                        }
                    }
                }

                if("suffixList".equals(key)){
                    List<String> suffixList = (List<String>) value;
                    for (String suffixStr : suffixList) {
                        String preFileName = fileName;
                        fileName = fileName + suffixStr;
                        if(!StringUtils.equals(preFileName,fileName)){
                            System.out.println("path:"+path+" fileName : "+preFileName+" → "+fileName+" ");
                        }
                    }
                }


                if("regexReplaceList".equals(key)){
                    List<String> regexReplaceList = (List<String>) value;
                    for (String regex : regexReplaceList) {
                        String preFileName = fileName;
                        fileName = fileName.replaceAll(regex,"");
                        if(!StringUtils.equals(preFileName,fileName)){
                            System.out.println("path:"+path+" fileName : "+preFileName+" → "+fileName+" ");
                        }
                    }
                }
            }


            f = new File(path + "\\" + oldFileName); //输出地址和原路径保持一致
            f.renameTo(new File(path + "\\" +  fileName));


            if(f.isDirectory()){
              String subPath =  path + "\\" +  fileName;
              multiRename(subPath,replacementChain);
            }

        }
        System.out.println("恭喜，批量重命名成功！");
    }

    public static void main(String[] args) {
        List<Map<String,String>> replaceList = new ArrayList<>();
        List<String> prefixList = new ArrayList<>();
        List<String> suffixList = new ArrayList<>();
        List<String> regexReplaceList = new ArrayList<>();
        regexReplaceList  = Arrays.asList(ReplacementChain.smallBracketsRegex,ReplacementChain.midBracketsRegex,ReplacementChain.bigBracketsRegex,ReplacementChain.strongBracketsRegex);

        //Map<String,String> replaceMap = new HashMap<>();
        //replaceMap.put("","");
        //replaceList.add(replaceMap);

        //prefixList = Arrays.asList("【xxm】");
        //suffixList = Arrays.asList("【倾力打造】");

        ReplacementChain replacementChain = new ReplacementChain();
        replacementChain.addRegulation(replaceList, prefixList,suffixList,regexReplaceList);
        //Rename.multiRename("I:\\java\\a项目\\2020年最新 springboot基础脚手架（笔记、源码、素材 完整)\\运行截图", replacementChain);
        Rename.multiRename("I:\\java\\springboot\\小马哥讲Spring核心编程思想 更新122节", replacementChain);
    }
}
