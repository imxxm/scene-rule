package cn.xxm.scene.bo;

import cn.xxm.scene.entity.Role;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @program: scene-rule
 * @link: 55864455@qq.com
 * @author: Mr.Xxm
 * @create: 2020-02-22 23:30
 **/
@Data
@ToString
public class UserBo implements Serializable {

    private static final long serialVersionUID = -643964711236154027L;

    private String id;
    private String username;
    private String email;
    private String mobile;
    private List<Role> role;
    //@JsonProperty("state")
    @JSONField(name="state")
    private boolean status;

}
